(function(window, document, $){
	"use strict";
	
	var NavBar = function( element ){
		this.$navBar = $(element);
		this.$navBarBody = this.$navBar.find('[data-role="nav-body"]');
		this.$navBarDeployer = this.$navBar.find('[data-role="nav-deployer"]');
		this.$navBarDeployer.on('click.NavBar', this.toggleNavBar.bind(this));
		this.touchSubmenus();
		//WATCH
		this.$extraBarBody = this.$navBar.find('[data-role="extra-body"]');
		this.$extraBarDeployer = this.$navBar.find('[data-role="extra-deployer"]');
		this.$extraBarDeployer.on('click.NavBar', this.toggleExtra.bind(this));

		return this;
	};

	NavBar.prototype = {
		toggleNavBar : function( event ){
			event.preventDefault();
			this.$navBarDeployer.toggleClass('deployed');
			this.$navBarBody.toggleClass('deployed');
			$('body').toggleClass('nav--deployed');

			this.$extraBarDeployer.removeClass('deployed');
			this.$extraBarBody.removeClass('deployed');
			$('body').removeClass('extra--deployed');
		},

		toggleExtra : function( event ){
			event.preventDefault();
			this.$extraBarDeployer.toggleClass('deployed');
			this.$extraBarBody.toggleClass('deployed');
			$('body').toggleClass('extra--deployed');

			this.$navBarDeployer.removeClass('deployed');
			this.$navBarBody.removeClass('deployed');
			$('body').removeClass('nav--deployed');
		},

		touchSubmenus : function(){
			if( !Modernizr.touchevents ){ return; }
			var $touchSubmenus = $('body').find('[data-role="touch-submenu"]');
			$touchSubmenus.on('click.touchSubmenus', '[data-role="touch-submenu-deployer"]', function(e){
				event.preventDefault();
				$(e.currentTarget).parent().toggleClass('deployed');
			});
		}
	};

	$.fn.navBar = function(){
		if( this.data('navBar') ){ return this.data('navBar'); }
		return this.each(function(i, el){
			$(el).data('navBar', (new NavBar(el)));
		});
	};

	// self initialization
	$(document).ready(function(){
		$('[data-module="nav-bar"]').navBar();
	});

}(window, document, jQuery));